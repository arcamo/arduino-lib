#include <stdio.h>
#include "manager.hpp"
#include "thing.hpp"
#include "string.hpp"

int main(int argc, char ** argv){
	arc::SmartThingFactory factory;
	arc::SmartThing<unsigned char,arc::String> * gpio  = factory.makeGPIO(1,'z');
	gpio->setHighLevel("0");
	gpio->setValue(1);
	gpio->setValue(0);
	gpio->setValue('Z');

	arc::ThingManagerBuilder builder;
	builder.gpio(1);

	delete gpio;
	return 1;
}