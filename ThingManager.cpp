#include "ThingManager.h"

void delete_thing(StandardThing * thing){
	delete thing;
}

ThingManager::ThingManager(): collection(){

}

ThingManager::~ThingManager(){
	collection.forEach(delete_thing);
}

void ThingManager::process(const char * const cmd, Print& output){
	if(cmd[0] == '?' && cmd[1] == '\0'){

	}
	else if(cmd[0] == '?'){
		this->processQueryString(&cmd[1],output);
	}
}

int strindex(const char * const str, char s){
	for(int i=0;str[i] != '\0';i+=1){
		if(str[i] == s){
			return i;
		}
	}
	return -1;
}

char * makeSubstring(const char * const str,int begin,int end){
	int size = end - begin;
	char * sub = new char[size+1];
	sub[size] = '\0';
	for(int i=0;i<size;++i){
		sub[i] = str[begin+i];
	}
	return sub;
}

void ThingManager::processQueryString(const char * const cmd, Print& output){
	int prefix = 0;
	int size = strlen(cmd);
	while(prefix < size){
		int index = strindex(&cmd[prefix],'&');
		if(index == -1){
			index = strlen(cmd);
		}
		else{
			index += prefix;
		}
		char * node = makeSubstring(cmd,prefix,index);
		int equalsIndex = strindex(node,'=');
		if(equalsIndex == -1){
			StringBuilder sb;
			StandardThing * thing = this->find(node);
			sb.append("{\"")
				.append(thing->getHighLevelId())
				.append("\":");
			thing->getHighLevelValue(sb);
			sb.append('}');
			sb.write(output);
		}
		else{
			node[equalsIndex] = '\0';
			StandardThing * thing = this->find(node);
			if (thing != NULL){
				thing->setHighLevelValue(&node[equalsIndex+1]);	
			}
		}
		prefix = index+1;
		delete[] node;	
	}
}

void ThingManager::process(Stream& input, Print& output){
	char buffer[32];
	if(input.available() < 1){
		return;
	}
	input.readBytesUntil('\n',buffer,32);
	this->process(buffer,output);
}


ThingManager& ThingManager::gpio(const byte& lId, const char * const hId, byte pin){
	GPIO<unsigned char,const char * const> * gpio = new GPIO<unsigned char,const char * const>(lId,hId,pin);
	collection.add((StandardThing*)gpio);
	return *this;
}

ThingManager& ThingManager::analog(const byte& lId, const char * const hId, byte pin){
	AnalogIO<unsigned char,const char * const> * analog = new AnalogIO<unsigned char,const char * const>(lId,hId,pin);
	collection.add((StandardThing*)analog);
	return *this;
}

ThingManager& ThingManager::servo(const byte& lId, const char * const hId, byte pin){
	ServoThing<unsigned char,const char * const> * servo = new ServoThing<unsigned char,const char * const>(lId,hId,pin);
	collection.add((StandardThing*)servo);
	return *this;
}


StandardThing * ThingManager::find(const char * const id){
	return this->collection.findByHighLevel(id);
}

StandardThing * ThingManager::find(const byte& id){
	return this->collection.findByLowLevel(id);
}