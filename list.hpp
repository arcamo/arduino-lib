#ifndef __ARC_LIST__
#define __ARC_LIST__

#include <stdlib.h>

namespace arc {


	template<typename T>
	class Iterator {
	public:
		virtual bool hasNext() const = 0;
		virtual T next() = 0;
	};


	template<typename T>
	class LinkedList {
	public:
		class Node{
		protected:
			T value;
			Node * next;
			Node * prev;

		public:
			
			Node(T value,Node * prev, Node * next);
			~Node(){};
			T operator*();
			Node& operator++();
			Node& operator--();
			bool hasNext();
			bool hasPrev();

			friend class LinkedList<T>;

		};

		class LinkedListIterator: public Iterator<T&> {
		protected:
			Node * node;
		public:
			LinkedListIterator(Node * node);
			T& next();
			bool hasNext() const;

		};

		LinkedList();
		virtual ~LinkedList();
		void add(T value);
		void forEach(void (*callback)(T value));
		unsigned int size() const;
		T * toArray() const;
		Node& getFirst();
		Node& getLast();

		Iterator<T&> iterate();

	private:


		unsigned int length =0;
		Node * first = NULL;
		Node * last = NULL;
		
	};

	template<typename T>
	LinkedList<T>::LinkedList(){

	}

	template<typename T>
	LinkedList<T>::~LinkedList(){
		Node * n = this->first;
		while( n != NULL){
			Node * oldN = n;
			n = n->next;
			delete oldN;
		}
	}

	template<typename T>
	void LinkedList<T>::forEach(void (*callback)(T value)){
		Node * n = this->first;
		while( n != NULL){
			callback(n->value);
			n = n->next;
		}	
	}

	template<typename T>
	unsigned int LinkedList<T>::size() const{
		return this->length;
	}

	template<typename T>
	T * LinkedList<T>::toArray() const{
		int i = 0;
		T * array = new T[this->length];
		Node * n = this->first;
		while( n != NULL){
			array[i] = n->value;
			i++;
			n = n->next;
		}
		return array;
	}

	template<typename T>
	void LinkedList<T>::add(T value){
		this->length += 1;
		Node * n = new Node(value,this->last,NULL);
		if(this->first == NULL){
			this->first = n;
			this->last = n;
		}
		else{
			this->last->next = n;
			this->last = n;
		}
	}

	template<typename T>
	typename LinkedList<T>::Node& LinkedList<T>::getFirst(){
		return *this->first;
	}

	template<typename T>
	typename LinkedList<T>::Node& LinkedList<T>::getLast(){
		return *this->last;
	}

	template<typename T>
	LinkedList<T>::Node::Node(T value, Node * prev,Node * next) : value(value){
		this->prev = prev;
		this->next = next;
	}

	template<typename T>
	T LinkedList<T>::Node::operator*(){
		return this->value;
	}

	template<typename T>
	bool LinkedList<T>::Node::hasNext(){
		return this->next != NULL;
	}

	template<typename T>
	bool LinkedList<T>::Node::hasPrev(){
		return this->prev != NULL;
	}

	template<typename T>
	typename LinkedList<T>::Node& LinkedList<T>::Node::operator++(){
		return *this->next;
	}

	template<typename T>
	typename LinkedList<T>::Node& LinkedList<T>::Node::operator--(){
		return *this->prev;
	}

	template<typename T>
	Iterator<T&> LinkedList<T>::iterate() {
		return LinkedList<T>::LinkedListIterator(this->first);
	}


	template<typename T>
	LinkedList<T>::LinkedListIterator::LinkedListIterator(LinkedList<T>::Node * node){
		this->node = node;
	}

	template<typename T>
	T& LinkedList<T>::LinkedListIterator::next(){
		Node * node = this->node;
		this->node = node->next;
		return **node;
	}

	template<typename T>
	bool LinkedList<T>::LinkedListIterator::hasNext() const{
		return this->node != NULL;
	}


}

#endif