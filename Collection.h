#ifndef __COLLECTION__
#define __COLLECTION__

#include <stdlib.h>
#include "Identifiable.h"


template<typename H>
inline bool equals(H a,H b){
	if(a == NULL && b == NULL){
		return true;
	}
	if(a != NULL){
		return a.equals(b);
	}
	return false;
}

template<>
inline bool equals(const char * const a,const char * const b){
	int i =0;
	bool ok = true;
	while(ok){
		ok = a[i] == b[i];
		if(a[i] == '\0' || b[i] == '\0'){
			break;
		}
		i++;
	}
	return ok;
}



template<typename T, typename L, typename H>
class ThingCollection {
private:
public:
	typedef void (*FConsumer)(T*);

	virtual void add(T  * thing) = 0;
	virtual T * findByLowLevel(L lId) = 0;
	virtual T * findByHighLevel(H hId) = 0;
	virtual void forEach(FConsumer callback) =0;
};


template<typename T, typename L,typename H>
class LinkedListThingCollection : public ThingCollection<T,L,H>{
private:
	class Node {
	public:
		Node * next;
		T * thing;
	};
	Node * first;
public:
	LinkedListThingCollection();
	~LinkedListThingCollection();
	void add(T * thing);
	T * findByLowLevel(L lId);
	T * findByHighLevel(H hId);
	void forEach(typename ThingCollection<T,L,H>::FConsumer callback);
};

template<typename T, typename L,typename H>
LinkedListThingCollection<T,L,H>::LinkedListThingCollection(){
	this->first = NULL;
}

template<typename T, typename L,typename H>
LinkedListThingCollection<T,L,H>::~LinkedListThingCollection(){
	LinkedListThingCollection<T,L,H>::Node * node = this->first;
	while(node!=NULL){
		LinkedListThingCollection<T,L,H>::Node * oldNode = node;
		node = node->next;
		delete oldNode;
	}
}

template<typename T, typename L,typename H>
void LinkedListThingCollection<T,L,H>::add(T * thing){
	LinkedListThingCollection<T,L,H>::Node * node = new LinkedListThingCollection<T,L,H>::Node();
	node->next = this->first;
	node->thing = thing;
	this->first = node;
}

template<typename T, typename L,typename H>
T * LinkedListThingCollection<T,L,H>::findByLowLevel(L lId){
	LinkedListThingCollection<T,L,H>::Node * node;
	for(node=this->first;node!=NULL;node = node->next){
		if(node->thing->getLowLevelId() == lId){
			return node->thing;
		}
	}
	return NULL;
}

template<typename T, typename L,typename H>
T * LinkedListThingCollection<T,L,H>::findByHighLevel(H hId){
	LinkedListThingCollection<T,L,H>::Node * node;
	for(node=this->first;node!=NULL;node = node->next){
		if(equals(node->thing->getHighLevelId(),hId)){
			return node->thing;
		}
	}
	return NULL;
}

template<typename T, typename L,typename H>
void LinkedListThingCollection<T,L,H>::forEach(typename ThingCollection<T,L,H>::FConsumer callback){
LinkedListThingCollection<T,L,H>::Node * node = this->first;
	while(node!=NULL){
		callback(node->thing);
		node = node->next;
	}	
}


typedef LinkedListThingCollection<StandardThing, unsigned char, const char * const> StandardThingCollection;



#endif