#include "RF24Adapter.hpp"

RF24Adapter::RF24Adapter(RF24 * radio){
	this->radio = radio;
}

size_t RF24Adapter::write(uint8_t data){
	char buff[2];
	buff[1] = '\0';
	buff[0] = data;
	if(this->radio->write(buff,1)){
		return 1;	
	}
	return -1;
}

size_t RF24Adapter::write(const uint8_t *buffer, size_t size){
	if(this->radio->write(buffer,size)){
		return size;
	}
	return -1;
}
