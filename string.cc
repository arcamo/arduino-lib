#include "string.hpp"
#include "stdio.h"

using namespace arc;

String test(){
	const char * const txt = "Um Texto Qualquer";
	return String(txt);
}

void test_indexOf(){
	String name("Ettore Leandro Tognoli");
	if(name.indexOf("E") != 0){
		printf("Fail\n");
	}
	if(name.indexOf(String("Ettore")) != 0){
		printf("Fail\n");
	}
	if(name.indexOf("Leandro") != 7){
		printf("Fail\n");
	}
	if(name.indexOf('y') != -1){
		printf("Fail\n");
	}
	if(name.indexOf("Barabara") != -1){
		printf("Fail\n");	
	}
	if(name.indexOf("Tognoli") != 15){
		printf("Fail\n");	
	}
}

int main(int argc,char**argv){
	test_indexOf();
	String name("Ettore Leandro Tognoli");
	String name1 = name;
	String name2 = name + name1;
	String strChar('e');
	String firstName = name.subString(0,6);
	String lastName = name.subString(-7);
	String strTest = test();
	StringBuilder sb(128);
	sb.append('a').append("bcdefghijklmnopqrstuvwxyz").append(" ").append(String(-10));
	sb.append('\n');
	sb.append(String(-57));
	sb.append('\t');
	sb.append(String(9123));
	String fromBuilder = (String)sb;
	for(int i =0;i<fromBuilder.size();i++){
		printf("%c",fromBuilder[i]);
	}
	printf("\n");
	return 1;
}