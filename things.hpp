#ifndef __ARC_THINGS__
#define __ARC_THINGS__

#include "thing.hpp"
#include "string.hpp"

#ifdef __linux__

#include "linux/Arduino.hpp"

#elif __AVR__

#include "Arduino.h"
#include "Servo.h"

#endif

namespace arc {

	class GPIO : public AbstractSmartThing<unsigned char,String>{	
	private:
		unsigned char pin;
	public:
		GPIO(unsigned char pin);
		GPIO(unsigned char pin, unsigned char value);
		GPIO& setValue(const unsigned char& value);
		unsigned char getValue() const;
	};


	class AnalogIO : public AbstractSmartThing<unsigned char,String>{
	private:
		unsigned char pin;
	public:
		AnalogIO(unsigned char pin);
		AnalogIO& setValue(const unsigned char& value);
		unsigned char getValue() const;
	};


	class ServoThing : public AbstractSmartThing<unsigned char,String>, public Servo {
	private:
		unsigned char lastValue;
	public:
		ServoThing(unsigned char pin);
		ServoThing(unsigned char pin, unsigned char value);
		virtual ~ServoThing();
		ServoThing& setValue(const unsigned char& value);
		unsigned char getValue() const;
	};

}

#endif