#include "thing.hpp"
#include "string.hpp"
#include <stdio.h>


int value;

void set(int i){
	value = i;
	printf("SET %d\n",i);
}

int get(){
	printf("GET %d\n",value);
	return value;
}



int main(int argc,char ** argv){
	arc::RegisterThing<int> rThing(&value);
	arc::CustomThing<int> cThing(get,set);
	
	cThing.setValue(10);
	rThing.setValue(100);
	rThing.getValue();
	cThing.getValue();

	arc::SmartThingAdapter<int,arc::String> adapter(cThing);
	adapter.setValue(10);
	adapter.getValue();
	adapter.setHighLevel(arc::String("999"));
	adapter.setValue(-78);
	arc::String highLevelValue = adapter.getHighLevel();
	char  * str = new char[highLevelValue.size()+1];
	highLevelValue.write(str);
	printf("%s\n", str);

}