#ifndef __ARC_MANAGER__
#define __ARC_MANAGER__

#include "thing.hpp"
#include "things.hpp"
#include "string.hpp"
#include "list.hpp"
#include "tuple.hpp"

namespace arc{

	class SmartThingFactory;
	class ThingManager;
	class ThingManagerBuilder;

	class SmartThingFactory {
	public:
		SmartThing<unsigned char,String> * makeGPIO(unsigned char pin, unsigned char value);
		SmartThing<unsigned char,String> * makeAnalogIO(unsigned char pin);
		SmartThing<unsigned char,String> * makeServo(unsigned char pin, unsigned char value);
		SmartThing<unsigned char,String> * makeCustom(unsigned char * address);
		SmartThing<unsigned char,String> * makeCustom(unsigned char (*get)(), void (*set)(unsigned char));
	};

	class ThingManagerBuilder {
	protected:
		unsigned int counter = 0;
		SmartThingFactory factory;
		LinkedList< Tuple<String, SmartThing<unsigned char,String>* > > * list;
	public:
		ThingManagerBuilder();
		~ThingManagerBuilder();
		ThingManager * build();

		ThingManagerBuilder& gpio(unsigned char pin);
		ThingManagerBuilder& gpio(const String& name, unsigned char pin);
		ThingManagerBuilder& gpio(const String& name, unsigned char pin, char value);

		
		ThingManagerBuilder& analog(unsigned char pin);
		ThingManagerBuilder& analog(const String& name, unsigned char pin);
		ThingManagerBuilder& analog(const String& name, unsigned char pin, unsigned char value);

		/*
		virtual ThingManagerBuilder& servo(unsigned char pin) = 0;
		virtual ThingManagerBuilder& servo(const String& name, unsigned char pin) = 0;
		virtual ThingManagerBuilder& servo(const String& name, unsigned char pin, unsigned char value) =0;

		virtual ThingManagerBuilder& custom(unsigned char (*get)(),void (*set)(unsigned char)) = 0;
		virtual ThingManagerBuilder& custom(const String& name,unsigned char (*get)(),void (*set)(unsigned char)) = 0;

		virtual ThingManagerBuilder& custom(unsigned char * value);
		virtual ThingManagerBuilder& custom(const String& name,unsigned char * value);


		virtual ~ThingManagerBuilder() = 0;
		*/
	};

	class ThingManager {
	protected:
		LinkedList< Tuple<String, SmartThing<unsigned char,String>* > > * list;
		ThingManager(LinkedList< Tuple<String, SmartThing<unsigned char,String>* > > * list);
		friend ThingManager * ThingManagerBuilder::build();
	public:
		virtual ~ThingManager();
	};

};

#endif