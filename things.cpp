#include "things.hpp"

namespace arc {

	GPIO::GPIO(unsigned char pin) : AbstractSmartThing<unsigned char,String>(){
		this->pin = pin;
	}

	GPIO::GPIO(unsigned char pin, unsigned char value) : AbstractSmartThing<unsigned char,String>(){
		this->pin = pin;
		this->setValue(value);
	}

	GPIO& GPIO::setValue(const unsigned char& v){
		char value = (char)v;
		switch(value){
			case 'z':
				pinMode(this->pin,INPUT);
				break;
			case 'Z':
				pinMode(this->pin,INPUT_PULLUP);
				break;
			case '0':
			case 0:
				pinMode(this->pin,OUTPUT);
				digitalWrite(this->pin,0);
				break;
			case '1':
			case 1:
				pinMode(this->pin,OUTPUT);
				digitalWrite(this->pin,1);
				break;
		}
		return *this;
	}

	unsigned char GPIO::getValue() const{
		return digitalRead(this->pin);
	}

	//---------------------

	AnalogIO::AnalogIO(unsigned char pin) : AbstractSmartThing<unsigned char,String>(){
		this->pin = pin;
	}
	
	AnalogIO& AnalogIO::setValue(const unsigned char& value){
		analogWrite(this->pin,value);
		return *this;
	}

	unsigned char AnalogIO::getValue() const{
		return analogRead(this->pin) >> 1;
	}

	//---------------------

	ServoThing::ServoThing(unsigned char pin) : AbstractSmartThing<unsigned char,String>(), Servo(){
		this->lastValue = 255;
		Servo::attach(pin);
	}

	ServoThing::ServoThing(unsigned char pin, unsigned char value) : AbstractSmartThing<unsigned char,String>(), Servo(){
		this->lastValue = value;
		Servo::attach(pin);
		this->setValue(value);
	}

	ServoThing::~ServoThing(){
		Servo::detach();
	}

	ServoThing& ServoThing::setValue(const unsigned char& value){
		this->lastValue = value;
		Servo::write(value);
		return *this;
	}

	unsigned char ServoThing::getValue() const{
		//return Servo::read();
		return this->lastValue;
	}


}