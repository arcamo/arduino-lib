CC := g++
OBJSUFFIX := hpp
OBJHSUFFIX := hpp

RUNSUFFIX := cc
RUNHSUFFIX := hh

#$@    Nome da regra. 
#$<    Nome da primeira dependência 
#$^    Lista de dependências
#$?    Lista de dependências mais recentes que a regra.
#$*    Nome do arquivo sem sufixo

CFLAGS := -pipe -std=c++11 -march=corei7 #-W -Wall
CLIBS := -L/usr/lib64
DIR := $(CURDIR)

CINCLUDES := -I/usr/include
CINCLUDES := -I$(DIR) $(CINCLUDES)


OSRC := $(shell find -type f -iname '*.$(OBJSUFFIX)' )
OHSRC := $(shell find -type f -iname '*.$(OBJHSUFFIX)' )
OBJECTS := $(foreach x, $(basename $(OSRC)), $(x).o)

RSRC := $(shell find -type f -iname '*.$(RUNSUFFIX)' )
RHSRC := $(shell find -type f -iname '*.$(RUNHSUFFIX)' )
RUNS := $(foreach x, $(basename $(RSRC)), $(x).run)

all: $(OBJECTS) $(RUNS)

clean:
	rm -f $(OBJECTS) $(RUNS)

%.o: %.cpp $(OHSRC)
	$(CC) $(CFLAGS) -c $< -o $@ $(CINCLUDES)


%.run: %.cc $(OBJECTS)
	$(CC) $(CFLAGS) $< -o $@ $(CLIBS) $(CINCLUDES) $(OBJECTS)

%.test: %.cc %.o
	$(CC) $(CFLAGS) $< -o $@ $(CLIBS) $(CINCLUDES) $*.o linux/Arduino.o

#%.run: %.$(RUNSUFFIX) $(RHSRC) $(OBJECTS)
#	$(CC) $(CFLAGS) $< -o $@ $(CLIBS) $(CINCLUDES) $(OBJECTS)
