#include <SPI.h>

#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

/* Sweep
 by BARRAGAN <http://barraganstudio.com> 
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://arduino.cc/en/Tutorial/Sweep
*/ 

#include <Servo.h> 
#include <RF24Adapter.h>
#include <ThingManager.h>

StandardThingManager manager;


RF24 radio(8,7);
RF24Adapter adapter(&radio);
void (*mode)(void);
void normal();
void panic();
StandardThing * elevator;
StandardThing * rudder;
StandardThing * aileronRight;
StandardThing * aileronLeft;
StandardThing * motor;


unsigned short wdt = 10000;
unsigned long lastms = 0;
unsigned long lastMsgTime = 0;

uint8_t address[6] = "arpl0";
uint8_t control[6] = "ctrl0";


void normal(){
  if( millis() - lastms >= 1000){
    lastms = millis();
    /*radio.stopListening();
    adapter.print("{\"ping\":\"pong\"}");
    radio.startListening();*/
    Serial.println("{\"ping\":\"pong\"}");
  }
  if(radio.available()){
    uint8_t len = radio.getDynamicPayloadSize();
    char data[64];    
    radio.read(&data,len);
    data[len] = '\0';
    lastMsgTime = millis();
    Serial.println(data);
    radio.stopListening();
    manager.process(data,adapter);
    radio.startListening();
  }
  else if(millis() - lastMsgTime >= wdt){
    lastMsgTime = millis();
    radio.powerDown();
    radio.enableDynamicPayloads();
    //radio.setCRCLength(RF24_CRC_8);  
    radio.openWritingPipe(control);
    radio.openReadingPipe(1,address);
    radio.powerUp();
    radio.startListening();
    Serial.println("{\"mode\":\"panic\"}");
  }
}

void panic(){
  if(radio.available()){
    uint8_t len = radio.getDynamicPayloadSize();
    char data[64];    
    radio.read(&data,len);
    data[len] = '\0';
    lastMsgTime = millis();
    radio.stopListening();
    manager.process(data,adapter);
    radio.startListening();
    mode = normal;
  }
  aileronLeft->setValue(90);
  aileronRight->setValue(90);
  motor->setValue(60);
  elevator->setValue(125);
  rudder->setValue(90);
}

void setup() 
{ 
  Serial.begin(115200);
  radio.begin();
  manager
    .servo(0,"m",A0)
    .servo(1,"e",A1)
    .servo(2,"r",A2)
    .servo(3,"ar",A3)
    .servo(4,"al",A4);
  //radio.setPALevel(RF24_PA_LOW);
  elevator = manager.find("e");
  rudder = manager.find("r");
  aileronRight = manager.find("ar");
  aileronLeft = manager.find("al");
  motor = manager.find("m");
  aileronLeft->setValue(90);
  aileronRight->setValue(90);
  motor->setValue(40);
  elevator->setValue(90);
  rudder->setValue(90);
  radio.enableDynamicPayloads();
  //radio.setCRCLength(RF24_CRC_8);  
  radio.openWritingPipe(control);
  radio.openReadingPipe(1,address);
  radio.powerUp() ;
  radio.startListening();
  mode = normal;
} 

void loop() 
{ 
  manager.process(Serial,Serial);
  mode();
} 

