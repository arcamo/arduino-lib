#include <arcamo.h>
#include <Servo.h>

const unsigned char POT = A1;
const unsigned char SERVO = A2;

arc::PID<double> pid(5.0,0.5,0.2);
Servo servo;


void setup(){
	Serial.begin(9600);
	servo.attach(SERVO);
	pid.setTarget(3.3);
}

double lowFilter = 0;

void loop(){
	arc::String name("ettore");
	delay(50);
	unsigned short potRaw = analogRead(POT);
	double voltage = potRaw  * 5.0 / 1023.0;
	lowFilter = (lowFilter * 3 + voltage)/4.0;
	Serial.print("Voltage: ");
	Serial.println(lowFilter,DEC);
	pid.setInput(lowFilter);
	unsigned char deg =  max(min(servo.read() + pid.getOutput(),180),0);
	servo.write(deg);
}
