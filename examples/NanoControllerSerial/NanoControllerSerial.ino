#include <SPI.h>

#include <nRF24L01.h>
#include <printf.h>
#include <RF24_config.h>
#include <RF24.h>

unsigned short wdt = 5000;
unsigned long lastMsgTime = 0;


RF24 radio(8,7);

byte address[6] = "ctrl0";
byte airplane[6] = "arpl0";

void setup() {
  Serial.begin(115200);
  radio.begin();
  //radio.setPALevel(RF24_PA_LOW);
  radio.enableDynamicPayloads();
  //radio.setCRCLength(RF24_CRC_8);
  radio.openWritingPipe(airplane);
  radio.openReadingPipe(1,address);
  radio.powerUp() ;
  radio.startListening();
}

void loop() {
  if(Serial.available() > 1 ){
    char sendData[64];
    int len = Serial.readBytesUntil('\n',sendData,64);
    radio.stopListening();
    if(!radio.write(sendData,len)){
      Serial.println("{\"error\":\"radio send error\"}");
    }
    lastMsgTime = millis();
    radio.startListening();
  }
  if(radio.available()){
    uint8_t len = radio.getDynamicPayloadSize();
    char receiveData[32+2];    
    radio.read(&receiveData,len);
    receiveData[len] = '\n';
    receiveData[len+1] = '\0';
    Serial.println(receiveData);
  }
  /*if(millis() - lastMsgTime >= wdt){
    radio.stopListening();
    radio.write("?",2);
    radio.startListening();
  }*/
}


