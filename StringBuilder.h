#ifndef __STRING_BUILDER__
#define __STRING_BUILDER__

#ifdef __linux__

#include "linux/Arduino.h"

#elif __AVR__

#include "Arduino.h"
#include "Print.h"
	
#endif

class StringBuilder {
protected:
	int counter;
	char buffer[128];
public:
	StringBuilder(){this->counter=0;}
	StringBuilder& append(const char);
	StringBuilder& append(const char * const);
	StringBuilder& append(char * const);
	StringBuilder& write(Print& output);
};

#endif