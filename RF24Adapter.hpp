#ifndef __RF24_ADAPTER__
#define __RF24_ADAPTER__


#include "Arduino.h"
#include "SPI.h"
#include "nRF24L01.h"
#include "RF24_config.h"
#include "RF24.h"


class RF24Adapter : public Print {
private:
	RF24 * radio;
public:
	RF24Adapter(RF24 * radio);
	virtual size_t write(uint8_t);
    virtual size_t write(const uint8_t *buffer, size_t size);
};


#endif