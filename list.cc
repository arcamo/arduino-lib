#include "list.hpp"

#include <stdio.h>

using namespace arc;


class A{
public:
	A(){printf("A()\n");};
	A(const A& a){printf("A(const A& a)\n");};
	~A(){printf("~A()\n");};
};


void printItem(int i){
	printf("%d\n",i);
}

int main(int argc,char ** argv){
	int i =1;
	LinkedList<int> list;
	list.add(1);
	list.add(2);
	list.add(3);
	list.forEach(printItem);
	int * v = list.toArray();
	for(int c =0;c<list.size();c++){
		printf("%d\n",v[c]);
	}
	delete v;

	for(LinkedList<int>::Node& n=list.getFirst();;n=++n){
		printf("Node: %d\n",*n);
		if(!n.hasNext()){
			break;
		}
	}

	LinkedList<A> listA;
	A a;
	listA.add(a);
	listA.add(a);
	LinkedList<A*> listARef;
	listARef.add(&a);
	listARef.add(&a);

	Interator<A&> it = listA.iterate()
	while(it.hasNext()){
		A& ai = it.next();
	}



	return 1;
}