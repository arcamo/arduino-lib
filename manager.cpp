#include "manager.hpp"
#include <stdio.h>


namespace arc{

	// SmartThingFactory

	SmartThing<unsigned char,String> *  SmartThingFactory::makeGPIO(unsigned char pin, unsigned char value){
		return new GPIO(pin,value);
	}

	SmartThing<unsigned char,String> * SmartThingFactory::makeAnalogIO(unsigned char pin){
		return new AnalogIO(pin);
	}

	SmartThing<unsigned char,String> * SmartThingFactory::makeServo(unsigned char pin, unsigned char value){
		return new ServoThing(pin,value);
	}

	SmartThing<unsigned char,String> * SmartThingFactory::makeCustom(unsigned char * address){
		return new SmartRegisterThing<unsigned char,String>(address);
	}

	SmartThing<unsigned char,String> * SmartThingFactory::makeCustom(unsigned char (*get)(), void (*set)(unsigned char)){
		return new SmartCustomThing<unsigned char,String>(get,set);
	}


	// ThingManagerBuilder

	ThingManagerBuilder::ThingManagerBuilder(){
		this->list = new LinkedList< Tuple<String, SmartThing<unsigned char,String>* > >();
	}

	ThingManagerBuilder::~ThingManagerBuilder(){
		if(this->list != NULL){
			delete this->list;
		}
	}

	ThingManager * ThingManagerBuilder::build(){
		ThingManager * manager =  new ThingManager(this->list);
		this->list = NULL;
		return manager;
	}

	ThingManagerBuilder& ThingManagerBuilder::gpio(unsigned char pin){
		SmartThing<unsigned char,String> * thing = this->factory.makeGPIO(pin,'Z');
		Tuple< String,SmartThing<unsigned char,String>* > tuple(String("gpio-")+String((const int)this->counter++),thing);
		this->list->add(tuple);
		return *this;
	}

	ThingManagerBuilder& ThingManagerBuilder::gpio(const String& name, unsigned char pin){
		SmartThing<unsigned char,String> * thing = this->factory.makeGPIO(pin,'Z');
		Tuple< String,SmartThing<unsigned char,String>* > tuple(name,thing);
		this->list->add(tuple);
		return *this;
	}

	ThingManagerBuilder& ThingManagerBuilder::gpio(const String& name, unsigned char pin, char value){
		SmartThing<unsigned char,String> * thing = this->factory.makeGPIO(pin,value);
		Tuple< String,SmartThing<unsigned char,String>* > tuple(name,thing);
		this->list->add(tuple);
		return *this;
	}

	ThingManagerBuilder& ThingManagerBuilder::analog(unsigned char pin){
		SmartThing<unsigned char,String> * thing = this->factory.makeAnalogIO(pin);
		Tuple< String,SmartThing<unsigned char,String>* > tuple(String("analog-")+String((const int)this->counter++),thing);
		this->list->add(tuple);
		return *this;
	}

	ThingManagerBuilder& ThingManagerBuilder::analog(const String& name, unsigned char pin){
		SmartThing<unsigned char,String> * thing = this->factory.makeAnalogIO(pin);
		Tuple< String,SmartThing<unsigned char,String>* > tuple(name,thing);
		this->list->add(tuple);
		return *this;
	}

	ThingManagerBuilder& ThingManagerBuilder::analog(const String& name, unsigned char pin, unsigned char value){
		SmartThing<unsigned char,String> * thing = this->factory.makeAnalogIO(pin);
		Tuple< String,SmartThing<unsigned char,String>* > tuple(name,thing);
		this->list->add(tuple);
		return *this;
	}


	// ThingManager

	ThingManager::ThingManager(LinkedList< Tuple<String, SmartThing<unsigned char,String>* > > * list){
		this->list = list;
	}

	ThingManager::~ThingManager(){
		delete this->list;
	}


	
}

