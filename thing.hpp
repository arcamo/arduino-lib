#ifndef __ARC_THING__
#define __ARC_THING__

namespace arc {

	template<typename T>
	class Thing {
	public:
		virtual ~Thing(){};
		virtual Thing<T>& setValue(const T& value) =0;
		virtual T getValue() const =0;
	};

	template<typename T>
	class RegisterThing : public Thing<T>{
	private:
		T * value;	
	public:
		RegisterThing(T * memoryAddress);
		~RegisterThing(){};
		RegisterThing<T>& setValue(const T& value);
		T getValue() const;
	};

	template<typename T>
	RegisterThing<T>::RegisterThing(T * memoryAddress){
		this->value = memoryAddress;
	}

	template<typename T>
	RegisterThing<T>& RegisterThing<T>::setValue(const T& value){
		*this->value = value;
		return *this;
	}

	template<typename T>
	T RegisterThing<T>::getValue() const {
		return *this->value;
	}

	template<typename T>
	class CustomThing : public Thing<T>{
	private:
		T (*getMethod)();
		void (*setMethod)(T);
	public:
		CustomThing(T (*getMethod)(), void (*setMethod)(T));
		~CustomThing(){};
		CustomThing<T>& setValue(const T& value);
		T getValue() const;
	};

	template<typename T>
	CustomThing<T>::CustomThing(T (*getMethod)(), void (*setMethod)(T)){
		this->getMethod = getMethod;
		this->setMethod = setMethod;
	}

	template<typename T>
	CustomThing<T>& CustomThing<T>::setValue(const T& value){
		this->setMethod(value);
	}

	template<typename T>
	T CustomThing<T>::getValue() const{
		return this->getMethod();
	}

	template<typename L,typename H>
	class SmartThing : public Thing<L> {
	public:
		virtual ~SmartThing(){};
		virtual SmartThing<L,H>& setHighLevel(const H& value) = 0;
		virtual H getHighLevel() const = 0;
		virtual SmartThing<L,H>& setValue(const L& value) = 0;
		virtual L getValue() const = 0;
	};

	template<typename L, typename H>
	class AbstractSmartThing : public SmartThing<L,H>{
	public:
		virtual AbstractSmartThing<L,H>& setHighLevel(const H& value);
		virtual H getHighLevel() const;
		virtual AbstractSmartThing<L,H>& setValue(const L& value) = 0;
		virtual L getValue() const = 0;
	};

	template<typename L, typename H>
	AbstractSmartThing<L,H>& AbstractSmartThing<L,H>::setHighLevel(const H& value){
		this->setValue((L)value);
	}

	template<typename L, typename H>
	H AbstractSmartThing<L,H>::getHighLevel() const {
		return (H)this->getValue();
	}

	template<typename L, typename H>
	class SmartRegisterThing : public RegisterThing<L>, public AbstractSmartThing<L,H> {
	public:
		SmartRegisterThing(L * address ) : RegisterThing<L>(address), AbstractSmartThing<L,H>(){};
		SmartRegisterThing& setValue(const L& value);
		L getValue() const;
	};

	template<typename L, typename H>
	SmartRegisterThing<L,H>& SmartRegisterThing<L,H>::setValue(const L& value){
		RegisterThing<L>::setValue(value);
		return *this;
	}

	template<typename L, typename H>
	L SmartRegisterThing<L,H>::getValue() const {
		return RegisterThing<L>::getValue();
	}

	template<typename L, typename H>
	class SmartCustomThing : public CustomThing<L>, public AbstractSmartThing<L,H> {
	public:
		SmartCustomThing(L (*getMethod)(), void (*setMethod)(L)) : CustomThing<L>(getMethod,setMethod) , AbstractSmartThing<L,H>(){};
		SmartCustomThing& setValue(const L& value);
		L getValue() const;
	};

	template<typename L, typename H>
	SmartCustomThing<L,H>& SmartCustomThing<L,H>::setValue(const L& value){
		CustomThing<L>::setValue(value);
		return *this;
	}

	template<typename L, typename H>
	L SmartCustomThing<L,H>::getValue() const {
		return CustomThing<L>::getValue();
	}


	template<typename L,typename H>
	class SmartThingAdapter : public SmartThing<L,H>{
	protected:
		Thing<L>& thing;
	public:
		SmartThingAdapter(Thing<L>& thing);
		~SmartThingAdapter(){};
		SmartThingAdapter<L,H>& setValue(const L& value);
		L getValue() const;
		virtual SmartThingAdapter<L,H>& setHighLevel(const H& value);
		virtual H getHighLevel() const;
	};

	template<typename L,typename H>
	SmartThingAdapter<L,H>::SmartThingAdapter(Thing<L>& thing) : thing(thing) {

	}

	template<typename L,typename H>
	SmartThingAdapter<L,H>& SmartThingAdapter<L,H>::setValue(const L& value){
		this->thing.setValue(value);
		return * this;
	}

	template<typename L,typename H>
	L SmartThingAdapter<L,H>::getValue() const{
		return this->thing.getValue();
	}

	template<typename L,typename H>
	SmartThingAdapter<L,H>& SmartThingAdapter<L,H>::setHighLevel(const H& value){
		this->setValue((L)value);
		return * this;
	}

	template<typename L,typename H>
	H SmartThingAdapter<L,H>::getHighLevel() const{
		return H(this->thing.getValue());
	}

};

#endif