#ifndef __ARC_TUPLE__
#define __ARC_TUPLE__


namespace arc {

	template<typename A,typename B>
	class Tuple {
	public:
		A a;
		B b;
		Tuple(A a, B b);
	};

	template<typename A, typename B>
	Tuple<A,B>::Tuple(A a, B b): a(a), b(b){
	}

}

#endif