#ifndef __ARC_STRING__
#define __ARC_STRING__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

namespace arc{

	class String;
	class StringBuilder;

	class StringBuilder {
	protected:
		int counter;
		char * buffer;
	public:
		StringBuilder(unsigned int size);
		StringBuilder& append(const char);
		StringBuilder& append(const char * const);
		StringBuilder& append(char * const);
		StringBuilder& append(const String& string);
		String toString() const;
		operator String() const;
	};



	class String {
	private:
		bool isConst;
		unsigned int length;
		const char * const buff;
	protected:
		String(const char * const str,int length,bool isConst);
		const char * const copyOrBuff() const;
		
	public:
		String(const char * const str);
		String(const char * const str, int length);
		String(const unsigned char i);
		String(const int);
		String(const char c);
		String(const String& string);
		~String();
		unsigned int size() const;
		String subString(int prefix) const;
		String subString(int prefix,int end) const;
		String replace(char s,char r) const;
		String operator+(const String& string) const;
		String operator*(const unsigned int m) const;
		char operator[](int index) const;
		int indexOf(char c) const;
		int indexOf(const String& string) const;
		bool equals(const String& string) const;
		bool startsWith(const String& string) const;
		void write(char * str) const;

		operator int() const;
		operator unsigned char() const;
		operator char() const;

		friend String StringBuilder::toString() const;
	};

}

#endif