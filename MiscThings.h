#ifndef __ARC_MISCTHINGS__
#define __ARC_MISCTHINGS__

#include <stdlib.h>
#include "StringBuilder.h"
#include "Identifiable.h"


typedef unsigned char byte;

#ifdef __linux__

#include "linux/Arduino.h"

#elif __AVR__

#include "Arduino.h"
#include "Servo.h"
	
#endif


char* itoa(int i, char b[]);

template<typename L,typename H>
class GPIO : public StaticIdentifiableThing<L,H,char>{	
private:
	byte pin;
public:
	GPIO(const L lId, const H hId, byte pin);
	void setValue(const char& value);
	void setHighLevelValue(const char * const strValue);
	void getHighLevelValue(StringBuilder& stringBuilder) const;
	const char getValue() const;
};


template<typename L,typename H>
GPIO<L,H>::GPIO(const L lId, const H hId, byte pin) : StaticIdentifiableThing<L,H,char>(lId,hId){
	this->pin = pin;
}

template<typename L,typename H>
void GPIO<L,H>::setValue(const char& value){
	switch(value){
		case 'z':
			pinMode(this->pin,INPUT);
			break;
		case 'Z':
			pinMode(this->pin,INPUT_PULLUP);
			break;
		case '0':
		case 0:
			pinMode(this->pin,OUTPUT);
			digitalWrite(this->pin,0);
			break;
		case '1':
		case 1:
			pinMode(this->pin,OUTPUT);
			digitalWrite(this->pin,1);
			break;
	}
}

template<typename L,typename H>
void GPIO<L,H>::setHighLevelValue(const char * const strValue){
	this->setValue(strValue[0]);
}

template<typename L,typename H>
const char GPIO<L,H>::getValue() const{
	return digitalRead(this->pin);
}

template<typename L,typename H>
void GPIO<L,H>::getHighLevelValue(StringBuilder& stringBuilder) const{
	char buff[2];
	stringBuilder.append(itoa(this->getValue(),buff));
}

template<typename L, typename H>
class AnalogIO : public StaticIdentifiableThing<L,H,byte>{
private:
	byte pin;
public:
	AnalogIO(const L lId,const H hId, byte pin);
	void setValue(const byte& value);
	void setHighLevelValue(const char * const);
	const byte getValue() const;
	void getHighLevelValue(StringBuilder& stringBuilder) const;
};

template<typename L, typename H>
AnalogIO<L,H>::AnalogIO(const L lId, const H hId, byte pin) : StaticIdentifiableThing<L,H,byte>(lId,hId){
	this->pin = pin;
}

template<typename L, typename H>
void AnalogIO<L,H>::setValue(const byte& value){
	analogWrite(this->pin,value);
}

template<typename L, typename H>
void AnalogIO<L,H>::setHighLevelValue(const char * const strValue){
	unsigned char v = (unsigned char) abs(atoi(strValue));
	this->setValue(v);
}

template<typename L, typename H>
const byte AnalogIO<L,H>::getValue() const{
	return analogRead(this->pin) >> 1;
}

template<typename L, typename H>
void AnalogIO<L,H>::getHighLevelValue(StringBuilder& stringBuilder) const{
	char buff[5];
	stringBuilder.append(itoa(analogRead(this->pin),buff));
}

template<typename L, typename H>
class ServoThing : public StaticIdentifiableThing<L,H,byte>, public Servo {
private:
	unsigned char lastValue;
public:
	ServoThing(const L lId, const H hId, byte pin);
	virtual ~ServoThing();
	void setValue(const byte& value);
	void setHighLevelValue(const char * const);
	const byte getValue() const;
	void getHighLevelValue(StringBuilder& stringBuilder) const;
};


template<typename L, typename H>
ServoThing<L,H>::ServoThing(const L lId, const H hId, byte pin) : StaticIdentifiableThing<L,H,byte>(lId,hId), Servo(){
	this->lastValue = -1;
	Servo::attach(pin);
}

template<typename L, typename H>
ServoThing<L,H>::~ServoThing(){
	Servo::detach();
}

template<typename L, typename H>
void ServoThing<L,H>::setValue(const byte& value){
	this->lastValue = value;
	Servo::write(value);
}

template<typename L, typename H>
void ServoThing<L,H>::setHighLevelValue(const char * const strValue){
	unsigned char v =(unsigned char)atoi(strValue);
	this->setValue(v);
}

template<typename L, typename H>
const byte ServoThing<L,H>::getValue() const{
	//return Servo::read();
	return this->lastValue;
}


template<typename L, typename H>
void ServoThing<L,H>::getHighLevelValue(StringBuilder& stringBuilder) const{
	char buff[4];
	stringBuilder.append(itoa(this->getValue(),buff));
}



#endif