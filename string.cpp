#include "string.hpp"
#include <stdio.h>


int ilog10(int i){
	if (i < 0){
		i = -i;
	}
	int log = 0;
	while( i >= 10){
		i = i / 10;
		log += 1;
	}
	return log;
}

char* itoa(int val){
	const char * const digits = "0123456789";
	int vlog = ilog10(val);
	int size = vlog + 1 + (val<0?1:0);
	char * buf = new char[size+1];
	buf[size] = '\0';
	if(val<0){
		buf[0] = '-';
	}
	if(val < 0){
		val = -val;
	}
	for(int i=0; val > 0;i++){
		int index = size-(i+1);
		buf[index] = digits[val % 10];
		val /= 10;		
	}
	return buf;
}

namespace arc {	

	String::String(const char * const str,int length, bool isConst) : buff(str){
		this->length = length;
		this->isConst = isConst;
	}

	String::String(const char c) : buff(new char[2]{c,'\0'}){
		this->isConst = false;
		this->length = 1;
	}

	String::String(const unsigned char i) : buff(itoa(i)){
		this->isConst = false;
		this->length = strlen(this->buff);
	}

	String::String(const int i) : buff(itoa(i)){
		this->isConst = false;
		this->length = strlen(this->buff);
	}

	String::String(const char *const str ) : buff(str){
		this->length = strlen(str);
		this->isConst = true;
	}

	String::String(const char *const str , int length) : buff(str){
		this->length = length;
		this->isConst = true;
	}

	String::String(const String& string) : buff(string.copyOrBuff()){
		this->length = string.length;
		this->isConst = string.isConst;
	}

	String::~String(){
		if(!this->isConst){
			delete[] this->buff;
		}
	}

	unsigned int String::size() const{
		return this->length;
	}

	String String::subString(int prefix) const{
		if (prefix < 0){
			prefix = this->length + prefix;
		}
		return String(&this->buff[prefix],this->length-prefix);
	}

	String String::subString(int prefix,int end) const{
		return String(&this->buff[prefix],this->length-prefix - (this->length -end));
	}

	String String::replace(char s, char r) const{
		char * newStr = new char[this->length+1];
		newStr[this->length] = '\0';
		for(int i=0;i<this->length;i++){
			if(this->buff[i] == s){
				newStr[i] = r;
			}
			else{
				newStr[i] = this->buff[i];	
			}
		}
		return String(newStr,this->length,false);
	}

	int String::indexOf(char c) const{
		for(int i=0;i<this->length;i++){
			if(this->buff[i] == c) return i;
		}
		return -1;
	}

	int String::indexOf(const String& string) const{
		for(int i=0;i<this->length;i++){
			if (this->buff[i] != string.buff[0]){
				continue;
			}
			if( i + string.length > this->length){
				continue;
			}
			for(int c =1;c<string.length;c++){
				if(this->buff[i+c] != string.buff[c]){
					continue;
				}
			}
			return i;
		}
		return -1;
	}

	bool String::startsWith(const String& string) const{
		if( this->length < string.length){
			return false;
		}
		for(int i=0;i<string.length;i++){
			if(this->buff[i] != string.buff[i]) return false;
		}
		return true;
	}

	const char * const String::copyOrBuff() const{
		if(this->isConst){
			return this->buff;
		}
		char * newStr = new char[this->length+1];
		newStr[this->length] = '\0';
		this->write(newStr);
		return newStr;
	}

	void String::write(char * str) const{
		for(int i=0;i<this->length;i++){
			str[i] = this->buff[i];
		}
	}

	String String::operator+(const String& string) const{
		unsigned int size = this->length + string.length;
		char * newStr = new char[size+1];
		newStr[size] = '\0';
		this->write(newStr);
		string.write(&newStr[this->length]);
		return String(newStr,size,false);
	}

	String String::operator*(const unsigned int m) const{
		unsigned int size = this->length * m;
		char * newStr = new char[size+1];
		newStr[size] = '\0';
		for(int i=0;i<m;i++){
			this->write(&newStr[i*this->length]);
		}
		return String(newStr,size,false);
	}

	char String::operator[](int index) const{
		if(index < 0 || index >= this->length){
			return '\0';
		}
		return this->buff[index];
	}

	String::operator int() const {
		return (int)atoi(this->buff);
	}

	String::operator unsigned char() const {
		return (unsigned char)atoi(this->buff);
	}

	String::operator char() const {
		return this->buff[0];
	}

	bool String::equals(const String& string) const{
		if(this->length != string.length){
			return false;
		}
		if(this->buff == string.buff){
			return true;
		}
		for(int i=0;i<string.length;i++){
			if (this->buff[i] != string.buff[i]) return false;
		}
		return true;
	}

	// ----------------------------------------------------------

	StringBuilder::StringBuilder(unsigned int size){
		this->buffer = new char[size];
		this->counter = 0;
	}

	StringBuilder& StringBuilder::append(const char c){
		this->buffer[this->counter] = c;
		this->counter += 1;
		return *this;
	}

	StringBuilder& StringBuilder::append(const char * const str){
		for(int i=0;str[i] != '\0' ;i+=1){
			this->buffer[this->counter] = str[i];
			this->counter += 1;
		}
		return *this;
	}

	StringBuilder& StringBuilder::append(char * const str){
		for(int i=0;str[i] != '\0' ;i+=1){
			this->buffer[this->counter] = str[i];
			this->counter += 1;
		}
		return *this;
	}

	StringBuilder& StringBuilder::append(const String& string){
		string.write(&this->buffer[this->counter]);
		this->counter += string.size();
	}

	String StringBuilder::toString() const{
		this->buffer[this->counter] = '\0';
		char * newStr = new char[this->counter+1];
		newStr[this->counter] = '\0';
		return String(newStr,this->counter,false);
	}

	StringBuilder::operator String() const{
		this->buffer[this->counter] = '\0';
		return String(this->buffer,this->counter);
	}

	

}