#include "thing.hpp"
#include "string.hpp"

namespace arc{

	template class RegisterThing<unsigned char>;
	template class RegisterThing<char>;


	template class SmartThingAdapter<unsigned char,String>;
	template class SmartThingAdapter<char,String>;
	
}