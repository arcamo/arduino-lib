#ifndef __IDENTIFIABLE__
#define __IDENTIFIABLE__

#include "StringBuilder.h"

/**
* L Low Level
* H High Level
* V Value
*/
template <typename L, typename H, typename V>
class IdentifiableThing{
public:
	virtual const L getLowLevelId() const = 0;
	virtual const H getHighLevelId() const = 0;
	virtual void setValue(const V& value) = 0;
	virtual void setHighLevelValue(const char * const strValue) = 0;
	virtual const V getValue() const = 0;
	virtual void getHighLevelValue(StringBuilder& stringBuilder) const =0;
	virtual ~IdentifiableThing(){};

};

template <typename L, typename H, typename V>
class StaticIdentifiableThing : public IdentifiableThing<L,H,V>{
private:
	L lowLevelId;
	H highLevelId;
public:
	StaticIdentifiableThing(const L lowLevelId, const H highLevelId);
	const L getLowLevelId() const;
	const H getHighLevelId() const;
	virtual void setValue(const V& value) = 0;
	virtual void setHighLevelValue(const char * const strValue) = 0;
	virtual void getHighLevelValue(StringBuilder& stringBuilder) const =0;
	virtual const V getValue() const = 0;
	virtual ~StaticIdentifiableThing(){};
};

template <typename L, typename H, typename V>
StaticIdentifiableThing<L,H,V>::StaticIdentifiableThing(const L lowLevelId, const H highLevelId)
:lowLevelId(lowLevelId), highLevelId(highLevelId) {

}

template <typename L, typename H, typename V>
const L StaticIdentifiableThing<L,H,V>::getLowLevelId() const{
	return this->lowLevelId;
}

template <typename L, typename H, typename V>
const H StaticIdentifiableThing<L,H,V>::getHighLevelId() const{
	return this->highLevelId;
}


typedef IdentifiableThing<unsigned char,const char * const,unsigned char> StandardThing;

#endif