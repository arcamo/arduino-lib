#ifndef __THING_MANAGER__
#define __THING_MANAGER__



#ifdef __linux__

#include "linux/Arduino.h"

#elif __AVR__

#include "Servo.h"
	
#endif


#include "StringBuilder.h"
#include "Identifiable.h"
#include "Collection.h"
#include "MiscThings.h"

typedef unsigned char byte;

class ThingManager  {
private:
	StandardThingCollection collection;
protected:
	void processQueryString(const char * const queryString,Print& output);
public:
	ThingManager();
	~ThingManager();

	void process(Stream& input, Print& output);
	void process(const char * const, Print& output);


	ThingManager& gpio(const byte& lId, const char * const hId, byte pin);
	ThingManager& analog(const byte& lId, const char * const hId, byte pin);
	ThingManager& servo(const byte& lId, const char * const hId, byte pin);
	StandardThing * find(const char * const id);
	StandardThing * find(const byte& id);

};

typedef ThingManager StandardThingManager;

#endif