#include "StringBuilder.h"

StringBuilder& StringBuilder::append(const char c){
	this->buffer[this->counter] = c;
	this->counter += 1;
	return *this;
}

StringBuilder& StringBuilder::append(const char * const str){
	for(int i=0;str[i] != '\0' ;i+=1){
		this->buffer[this->counter] = str[i];
		this->counter += 1;
	}
	return *this;
}

StringBuilder& StringBuilder::append(char * const str){
	for(int i=0;str[i] != '\0' ;i+=1){
		this->buffer[this->counter] = str[i];
		this->counter += 1;
	}
	return *this;
}

StringBuilder& StringBuilder::write(Print& output){
	this->buffer[this->counter] = '\0';
	output.write(this->buffer,this->counter);
	return *this;
}