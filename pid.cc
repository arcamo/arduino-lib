#include <stdlib.h>
#include <stdio.h>
#include "pid.hpp"

using namespace arc;


int main(int argc,char ** argv){
	unsigned long t = 0;
	PID<double> pid(1.0,0.5,0.9);
    pid.setInput(0, t);
    double g = 9.8;
    double ac = 0;  // aceleracao m/s²
    double ds = 0;  // velocidade
    int inc = 10;  // incremento 10ms

    pid.setTarget(30); // 1 m/s
   	for(int i=0;i<1000;i++){
   		t += inc;
   		pid.setInput(ds,t);
   		double result = pid.getOutput();
   		ac += result; // ajusta acelerador
   		ds += (ac - g) * ((double)inc / 1000.0); // calcula velocidade resultante
   		printf("Time:%dms\tV:%f\tAC:%f\tO:%f\n",t,ds,ac,result);
   	}
}